# Development workflow

Onionspray development guidelines and workflow are listed here.

## Release procedure

Release cycle workflow.

### Version update

Set the version number:

    ONIONSPRAY_VERSION=1.6.1

Update the version in some files, like:

    $EDITOR onionspray

### Register the changes

Update the ChangeLog:

    $EDITOR docs/changelog.md

Commit and tag:

    git diff # review
    git commit -a -m "Feat: Onionspray $ONIONSPRAY_VERSION"
    git tag -s v${ONIONSPRAY_VERSION} -m "Onionspray $ONIONSPRAY_VERSION"

Push changes and tags. Example:

    git push origin        && git push upstream
    git push origin --tags && git push upstream --tags

### Announcement

Announce the new release:

* Post a message to the [Tor Forum][].
* Send a message to the [tor-announce][] mailing list ONLY in special cases,
  like important security issues.

Template:

```
Subject: [RELEASE] Onionspray [security] release $ONIONSPRAY_VERSION

Greetings,

We just released Onionspray $ONIONSPRAY_VERSION.

[This release fixes a security issue. Please upgrade as soon as possible!]

ChangeLog is below.

$CHANGELOG
```

[tor-announce]: https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-announce
[Tor Forum]: https://forum.torproject.org
