#!/bin/sh -x

MAKE=make
keyserver="keyserver.ubuntu.com" # standard

# To ensure all software is properly built, make sure that /sbin is available
# at the PATH` environment variable.
#
# This is especially valid for LuaJIT, whose compilation might fail if
# /sbin is not on PATH with the `you need to have ldconfig in your PATH
# env when enabling luajit` error message:
# https://stackoverflow.com/questions/75568064/openresty-you-need-to-have-ldconfig-in-your-path-env-when-enabling-luajit
export PATH=$PATH:/sbin

CustomiseVars() {
    install_dir=$opt_dir/dist/$tool
    tool_tarball=`basename "$tool_url"`
    tool_checksum=`basename "$tool_checksum_url"`
    tool_sig=`basename "$tool_sig_url"`
    tool_dir=`basename "$tool_tarball" .tar.gz`
}

SetupForBuild() {
    test -f "$tool_tarball" || curl -o "$tool_tarball" "$tool_url" || exit 1
    test -f "$tool_sig" || curl -o "$tool_sig" "$tool_sig_url" || exit 1
    test -z "$tool_checksum" || test -f "$tool_checksum" || curl -o "$tool_checksum" "$tool_checksum_url" || exit 1
    test -z "$tool_checksum" || $tool_checksum_command "$tool_checksum" || exit 1
    gpg --keyserver "hkp://$keyserver:80" --recv-keys $tool_signing_keys || exit 1
    gpg --verify "$tool_sig" || exit 1
    test -d "$tool_dir" || tar zxf "$tool_tarball" || exit 1
    cd $tool_dir || exit 1
}

BuildAndCleanup() {
    $MAKE || exit 1
    $MAKE install || exit 1
    cd $opt_dir || exit 1
    for x in $tool_link_paths ; do ln -sf "$install_dir/$x" || exit 1 ; done

    if [ "$ONIONSPRAY_KEEP_SOURCES" != "1" ]; then
      rm -rf "$tool_tarball" "$tool_sig" "$tool_dir" "$tool_checksum" || exit 1
    fi
}

# ------------------------------------------------------------------

SetupOpenRestyVars() {
    tool="openresty"
    tool_version="1.25.3.1"
    tool_signing_keys="25451EB088460026195BD62CB550E09EA0E98066" # this is the full A0E98066 signature
    tool_url="https://openresty.org/download/$tool-$tool_version.tar.gz"
    tool_sig_url="https://openresty.org/download/$tool-$tool_version.tar.gz.asc"
    tool_link_paths="nginx/sbin/nginx"
}

ConfigureOpenResty() { # this accepts arguments
    or_mods="https://github.com/yaoweibin/ngx_http_substitutions_filter_module.git"
    or_opts="--with-http_sub_module --with-http_v2_module" # someday, redo this in lua
    or_mod_list=""

    for mod_url in $or_mods ; do
        mod_dir=`basename $mod_url .git`
        if [ -d "$mod_dir" ] ; then
            ( cd "$mod_dir" ; exec git pull ) || exit 1
        else
            git clone "$mod_url" || exit 1
        fi
        or_mod_list="$or_mod_list --add-module=$mod_dir"
    done

    ./configure --prefix="$install_dir" $or_opts $or_mod_list "$@" || exit 1
}

# Ref. https://openresty.org/en/linux-packages.html
SetupOpenRestyAptRepository() {
  # Get the distro name
  distro="$1"

  # Set the keyring name
  keyring="openresty-archive-keyring.gpg"

  # Determine architecture
  if [ "$distro" = "ubuntu" ]; then
    arch="`dpkg --print-architecture`"
  else
    arch="`uname -m`"
  fi

  # Determine release
  codename="`lsb_release -sc`"

  # Install APT keys
  keyring_folder="/usr/share/keyrings"
  keyring_opt="$opt_dir/apt/keyrings"
  test -d "/usr/share/keyrings" || exit 1
  sudo touch ${keyring_folder}/${keyring} || exit 1
  sudo chown root: ${keyring_folder}/${keyring} || exit 1
  cat ${keyring_opt}/${keyring} | sudo tee ${keyring_folder}/${keyring} > /dev/null || exit 1

  # Determine repository suffix
  if [ "$arch" = "arm64" ] || [ "$arch" = "aarch64" ]; then
    openresty_repo_suffix="arm64/"
  else
    openresty_repo_suffix=""
  fi

  # Add a repository entry
  if [ "$distro" = "ubuntu" ]; then
    echo "deb [arch=${arch} signed-by=${keyring_folder}/${keyring}] http://openresty.org/package/ubuntu ${codename} main" | sudo tee /etc/apt/sources.list.d/openresty.list > /dev/null || exit 1
  else
    echo "deb [signed-by=${keyring_folder}/${keyring}] http://openresty.org/package/${openresty_repo_suffix}debian ${codename} openresty" | sudo tee /etc/apt/sources.list.d/openresty.list || exit 1
  fi
}

# ------------------------------------------------------------------

SetupTorVars() {
    tool="tor"
    tool_version="0.4.8.10"
    tool_signing_keys="514102454D0A87DB0767A1EBBE6A0531C18A9179 B74417EDDF22AC9F9E90F49142E86A2A11F48D36 2133BC600AB133E1D826D173FE43009C4607B1FB"
    tool_url="https://dist.torproject.org/$tool-$tool_version.tar.gz"
    tool_checksum_url="https://dist.torproject.org/$tool-$tool_version.tar.gz.sha256sum"
    tool_checksum_command="sha256sum -c"
    tool_sig_url="https://dist.torproject.org/$tool-$tool_version.tar.gz.sha256sum.asc"
    tool_link_paths="bin/$tool"
}

ConfigureTor() { # this accepts arguments
    ./configure --enable-gpl --prefix="$install_dir" "$@" || exit 1
}

SetupTorAptRepository() {
  # Set the keyring name
  keyring="tor-archive-keyring.gpg"

  # Determine release
  codename="`lsb_release -sc`"

  # Install APT keys
  keyring_folder="/usr/share/keyrings"
  keyring_opt="$opt_dir/apt/keyrings"
  test -d "/usr/share/keyrings" || exit 1
  sudo touch ${keyring_folder}/${keyring} || exit 1
  sudo chown root: ${keyring_folder}/${keyring} || exit 1
  cat ${keyring_opt}/${keyring} | sudo tee ${keyring_folder}/${keyring} > /dev/null || exit 1

  # Add a repository entry
  if [ "codename" = "jammy" ]; then
    echo "deb https://deb.torproject.org/torproject.org ${codename} main" | sudo tee /etc/apt/sources.list.d/tor.list || exit 1
  else
    echo "deb [signed-by=${keyring_folder}/${keyring}] https://deb.torproject.org/torproject.org ${codename} main" | sudo tee /etc/apt/sources.list.d/tor.list || exit 1
  fi
}

# ------------------------------------------------------------------
