# Onionspray

![Onionspray Logo](docs/assets/logo.jpg "Onionspray")

Onionspray is a tool to setup [Onion Services][] for existing websites, working
as a HTTPS rewriting proxy. It's a fork of Alec Muffett's [EOTK][], with many
enhancements but retaining compatibility, and relying on [C Tor][] until an
alternative in [Arti][] is available.

[Onion Services]: https://community.torproject.org/onion-services/
[EOTK]: https://github.com/alecmuffett/eotk
[C Tor]: https://gitlab.torproject.org/tpo/core/tor
[Arti]: https://arti.torproject.org

## Documentation

Please refer to the [documentation folder](docs) or access the website at
[https://community.torproject.org/onion-services/ecosystem/apps/web/onionspray/][].

[https://community.torproject.org/onion-services/ecosystem/apps/web/onionspray/]: https://community.torproject.org/onion-services/ecosystem/apps/web/onionspray/
